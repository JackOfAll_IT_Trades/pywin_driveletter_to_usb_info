
import os
import os.path as path

import sys
import string
import argparse

import winreg
import subprocess


### HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\DeviceContainers\{ad618003-aaed-11eb-929d-f14dbdc0ddf6}\BaseContainers\{ad618003-aaed-11eb-929d-f14dbdc0ddf6}
DEBUG_LOG_FIELD_VALUES = False

#
#	DosDevices_ValueName  = "\\DosDevices\\" + driveLetter
#
#	print("PyWin_DriveLetter_To_USB_Info:  Querying MountedDevices key for value '{0}'...".format(DosDevices_ValueName))
#
#	if DosDevices_ValueType != winreg.REG_BINARY:
#		regTypeErrorMessageFormat = "Error:  MountedDevices value '{0}' was the wrong type!  (Got '{1}', expected '{2}'.)"
#		regTypeErrorMessageString = formatErrorMessageFormat.format( DosDevices_ValueName,
#																	 DosDevices_ValueType,
#																	 winreg.REG_BINARY )
#		print(regTypeErrorMessageString)
#		sys.exit(-40)
#	#
#	# END if DosDevices_ValueType != winreg.REG_BINARY: ...
#
#	DosDevices_ValueString = DosDevices_ValueData.decode("UTF-16LE")
#
#	DosDevices_ValueMessageFormat = "PyWin_DriveLetter_To_USB_Info:  MountedDevices key: value '{0}' = '{1}'..."
#	DosDevices_ValueMessageString = DosDevices_ValueMessageFormat.format(DosDevices_ValueName, DosDevices_ValueString)
#
#	print(DosDevices_ValueMessageString)
#


def Parse_Tabular_Data_Fields(headerLine) :
	#
	# Parse the double-space-separated field names from the first line:
	#
	fieldStartIndexes = []
	fieldNames = []

	headerLineLen = len(headerLine)

	bCurrentlyInFieldName  = False
	currentFieldNameString = ""
	previousFieldNameEnd   = 0
	currentFieldNameStart  = 0

	for currentIndex in range(0, headerLineLen-1) :
		currentChar  = headerLine[currentIndex]
		upcomingChar = headerLine[currentIndex+1]

		if bCurrentlyInFieldName and \
		(  currentChar in string.whitespace ) and \
		( upcomingChar in string.whitespace ) :
			#
			# We found a double-space separator!  That means we've finished processing the current field name.
			# Save it off, and update our internal tracking variables:
			#
			fieldNames.append(currentFieldNameString)
			bCurrentlyInFieldName  = False
			currentFieldNameString = ""
			previousFieldNameEnd   = currentIndex
			currentFieldNameStart  = 0
			continue
		#
		# END if bCurrentlyInFieldName and (currentChar in string.whitespace ) and (upcomingChar in string.whitespace) : ...

		if (not bCurrentlyInFieldName) and (currentChar not in string.whitespace) :
			#
			# We were *not* in a field name, but it looks like we just found the start of the next one!
			# Update our internal tracking variables:
			#
			bCurrentlyInFieldName = True
			currentFieldNameStart = currentIndex
			fieldStartIndexes.append(currentFieldNameStart)
		#
		# END if (not bCurrentlyInFieldName) and (currentChar not in string.whitespace) : ...

		if bCurrentlyInFieldName:
			#
			# We're in the middle of a field name, and we just found another character.
			# Add it to the currentFieldNameString:
			#
			currentFieldNameString += currentChar
		#
		# END if bCurrentlyInFieldName: ...

		continue
	#
	# END for currentIndex in range(0, firstLineLen-1) : ...

	return (fieldNames, fieldStartIndexes)
#
# END Parse_Tabular_Data_Fields(inputHeaderLine) : ...


def Parse_Tabular_Data(inputData) :
	inputLines = inputData.splitlines()

	headerLine = inputLines[0]

	(fieldNames, fieldStartIndexes) = Parse_Tabular_Data_Fields(headerLine)
	numFields = len(fieldNames)
	if len(fieldStartIndexes) != numFields:
		fieldLenMismatchMessageFormat = "Error:  The 'fieldNames' list [{0}] is not the same length as the 'fieldStartIndexes' list! [{1}]"
		fieldLenMismatchMessageString = fieldLenMismatchMessageFormat.format( len(fieldNames), len(fieldStartIndexes) )
		print(fieldLenMismatchMessageString)
		sys.exit(-40)
	#
	# END if len(fieldStartIndexes) != numFields: ...

	returnData_dictList = []

	for currentLine in inputLines[1:] :
		currentLine_dataDict = {}

		if DEBUG_LOG_FIELD_VALUES:
			print("")
			print("Parse_Tabular_Data() -- Beginning parsing of data record line, will store in currentLine_dataDict...")
			print("")
		#
		# END if DEBUG_LOG_FIELD_VALUES: ...


		currentPosition = 0
		for i in range(numFields) :
			if currentPosition >= len(currentLine) :
				break
			#
			# END if currentPosition >= len(currentLine) : ...

			fieldName = fieldNames[i]

			# Get field text:
			#
			fieldText = None
			if i >= (numFields - 1) :
				fieldText = currentLine[currentPosition:]
			else:
				currentField_EndPosition = fieldStartIndexes[i + 1]
				fieldText = currentLine[currentPosition:currentField_EndPosition]
				currentPosition = currentField_EndPosition
			#
			# END if i >= (numFields - 1) : ...

			fieldText = fieldText.strip()

			if DEBUG_LOG_FIELD_VALUES:
				logMessageFormat = 'Parse_Tabular_Data() -- \tSetting field value: currentLine_dataDict[{0}] = "{1}"...'
				logMessageString = logMessageFormat.format(fieldName, fieldText)
				print(logMessageString)
			#
			# END if DEBUG_LOG_FIELD_VALUES: ...

			currentLine_dataDict[fieldName] = fieldText
		#
		# END for i in range(numFields) : ...

		if DEBUG_LOG_FIELD_VALUES:
			print("")
			print("Parse_Tabular_Data() -- Finishing parsing of data record line, appending to returnData...")
			print("")
		#
		# END if DEBUG_LOG_FIELD_VALUES: ...

		if len( currentLine_dataDict.keys() ) > 0:
			#
			# Don't bother appending empty dictionaries -- just skip them:
			# (These happen when there are blank lines in the output.)
			#
			returnData_dictList.append(currentLine_dataDict)
		#
		# END if len(currentLine_dataDict.keys) > 0: ...

	#
	# END for inputLine in inputLines[1:] : ...

	return returnData_dictList
#
# END Parse_Tabular_Data(inputData) : ...


def Match_Key_ValueSuffix_And_Lookup_Other_Key( DictionaryList=None,
												MatchKey=None, MatchValueSuffix=None,
												ReturnKeyToLookup=None ) :
	matchingDictList = []

	for currentDictionary in DictionaryList:
		currentMatchKeyValue = currentDictionary[MatchKey]
		if currentMatchKeyValue.endswith(MatchValueSuffix) :
			matchingDictList.append(currentDictionary)
		#
		# END if currentMatchKeyValue.endswith(MatchValueSuffix) : ...

	#
	# END for currentDictionary in DictionaryList: ...

	if len(matchingDictList) == 0:
		warningMessageFormat = "Match_Key_ValueSuffix_And_Lookup_Other_Key() -- No dictionaries have a key [{0}] with a value that ends with [{1}]!"
		warningMessageString = warningMessageFormat.format(MatchKey, MatchValueSuffix)
		print(warningMessageString)
		return None
	#
	# END if len(matchingDictList) == 0: ...

	if len(matchingDictList) == 1:
		matchingDictionary = matchingDictList[0]
		valueToReturn = matchingDictionary[ReturnKeyToLookup]
		return valueToReturn
	#
	# END if len(matchingDictList) == 1: ...

	if len(matchingDictList) > 1:
		warningMessageFormat = "Match_Key_ValueSuffix_And_Lookup_Other_Key() -- Multiple dictionaries ([{0}] of them, in fact!) have a key [{0}] with a value that ends with [{1}]!"
		warningMessageString = warningMessageFormat.format(len(matchingDictList), MatchKey, MatchValueSuffix)
		print(warningMessageString)

		returnValueList = []
		for currentMatchingDict in matchingDictList:
			currentValueToReturn = currentMatchingDict[ReturnKeyToLookup]

			returnValueList.append(currentValueToReturn)
		#
		# END for currentMatchingDict in matchingDictList: ...

		return returnValueList
	#
	# END if len(matchingDictList) == 0: ...

	print("Match_Key_ValueSuffix_And_Lookup_Other_Key() -- ERROR:  Should never get here!")
	return None
#
# END Match_Key_Value_And_Lookup_Other_Key( DictionaryList=None, ... ) : ...


def Convert_DriveLetter_To_USB_Info(driveLetter) :
	(DiskDrive_exitCode, DiskDrive_output) = \
		subprocess.getstatusoutput("WMIC DiskDrive GET")

	(DiskDriveToDiskPartition_exitCode, DiskDriveToDiskPartition_output) = \
		subprocess.getstatusoutput("WMIC PATH Win32_DiskDriveToDiskPartition GET")

	(LogicalDiskToPartition_exitCode, LogicalDiskToPartition_output) = \
		subprocess.getstatusoutput("WMIC PATH Win32_LogicalDiskToPartition GET")

	print("PyWin_DriveLetter_To_USB_Info:  Parsing WMI DiskDrive data...")
	DiskDrive_dataDictList   = Parse_Tabular_Data(DiskDrive_output)

	print("PyWin_DriveLetter_To_USB_Info:  Parsing WMI Win32_DiskDriveToDiskPartition data...")
	Partition_dataDictList   = Parse_Tabular_Data(DiskDriveToDiskPartition_output)

	print("PyWin_DriveLetter_To_USB_Info:  Parsing WMI Win32_LogicalDiskToPartition data...")
	LogicalDisk_dataDictList = Parse_Tabular_Data(LogicalDiskToPartition_output)

	driveLetterSearchString = '"' + driveLetter + '"'

	driveLetterToPartition = Match_Key_ValueSuffix_And_Lookup_Other_Key( DictionaryList=LogicalDisk_dataDictList,
																		 MatchKey='Dependent',
																		 MatchValueSuffix=driveLetterSearchString,
																		 ReturnKeyToLookup='Antecedent' )
	if driveLetterToPartition is None:
		warningMessageFormat = "PyWin_DriveLetter_To_USB_Info:  ERROR -- Unable to map drive letter ({0}) to partition!  Returning..."
		warningMessageString = warningMessageFormat.format(driveLetterSearchString)
		print(warningMessageString)
		return None
	#
	# END if driveLetterToPartition is None: ...

	partitionStringPosition = driveLetterToPartition.rindex("=")
	partitionString = driveLetterToPartition[ partitionStringPosition+1 : ]

	diskDriveToPartition = Match_Key_ValueSuffix_And_Lookup_Other_Key( DictionaryList=Partition_dataDictList,
																	   MatchKey='Dependent',
																	   MatchValueSuffix=partitionString,
																	   ReturnKeyToLookup='Antecedent' )

	if diskDriveToPartition is None:
		warningMessageFormat = "PyWin_DriveLetter_To_USB_Info:  ERROR -- Unable to map disk drive <{0}> to partition!  Returning..."
		warningMessageString = warningMessageFormat.format(partitionString)
		print(warningMessageString)
		return None
	#
	# END if diskDriveToPartition is None: ...

	diskDriveStringPosition = diskDriveToPartition.rindex("=")
	diskDriveString = diskDriveToPartition[ diskDriveStringPosition+1 : ]

	diskDrive_noQuotes = diskDriveString.strip('"')


	diskDrive_PNPDeviceID = Match_Key_ValueSuffix_And_Lookup_Other_Key( DictionaryList=DiskDrive_dataDictList,
																		MatchKey='DeviceID',
																		MatchValueSuffix=diskDrive_noQuotes,
																		ReturnKeyToLookup='PNPDeviceID' )

	print("")
	PNP_Info_Format = "PNP device info. for [{0}] drive = [{1}]"
	PNP_Info_String = PNP_Info_Format.format( args.USB_DriveLetter, diskDrive_PNPDeviceID )
	print(PNP_Info_String)

	RegHive_HKLM = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)

	CurrentControlSet_Enum_RootPath = r"SYSTEM\CurrentControlSet\Enum"
	DiskDevice_RegKeyPath = CurrentControlSet_Enum_RootPath + "\\" + diskDrive_PNPDeviceID

	RegKey_DiskDevice = winreg.OpenKey(RegHive_HKLM, DiskDevice_RegKeyPath)

	(ContainerID_ValueData, ContainerID_ValueType) = winreg.QueryValueEx( RegKey_DiskDevice, "ContainerID" )

	ContainerID_Message_Format = "Read ContainerID [{0}] from device's PNP Registry key!"
	ContainerID_Message_String = ContainerID_Message_Format.format( ContainerID_ValueData )
	print(ContainerID_Message_String)

	CurrentControlSet_Enum_USB_Path = CurrentControlSet_Enum_RootPath + "\\" + "USB"

	ContainerID_Search_Msg_Format = "Searching for matching ContainerID in [{0}] section of the Registry..."
	ContainerID_Search_Msg_String = ContainerID_Search_Msg_Format.format( CurrentControlSet_Enum_USB_Path )
	print(ContainerID_Search_Msg_String)

	RegKey_Enum_USB = winreg.OpenKey(RegHive_HKLM, CurrentControlSet_Enum_USB_Path)

	(count_Enum_USB_subKeys, count_Enum_USB_values, Enum_USB_lastModifiedTimestamp) = \
		winreg.QueryInfoKey(RegKey_Enum_USB)

	Matching_ContainerID_RegKeys_PathList = []

	for i in range(count_Enum_USB_subKeys) :
		VID_PID_subkey_name = winreg.EnumKey(RegKey_Enum_USB, i)
		RegKey_VID_PID_subkey = winreg.OpenKey(RegKey_Enum_USB, VID_PID_subkey_name)

		(count_SerialNum_subKeys, count_VID_PID_values, VID_PID_lastModifiedTimestamp) = \
			winreg.QueryInfoKey(RegKey_VID_PID_subkey)

		for j in range(count_SerialNum_subKeys) :
			SerialNum_subkey_name = winreg.EnumKey(RegKey_VID_PID_subkey, j)
			RegKey_SerialNum_subkey = winreg.OpenKey(RegKey_VID_PID_subkey, SerialNum_subkey_name)

			(currentSubkey_ContainerID, currentSubkey_ContainerID_ValueType) = \
				winreg.QueryValueEx(RegKey_SerialNum_subkey, "ContainerID")

			if ContainerID_ValueData.lower() == currentSubkey_ContainerID.lower() :
				fullKeyPath = CurrentControlSet_Enum_RootPath + "\\" + "USB"+ "\\" + VID_PID_subkey_name + "\\" + SerialNum_subkey_name

				ContainerID_Found_Msg_Format = "Found matching ContainerID [{0}] in key [{1}]]!"
				ContainerID_Found_Msg_String = ContainerID_Found_Msg_Format.format( currentSubkey_ContainerID, fullKeyPath )
				print(ContainerID_Found_Msg_String)

				Matching_ContainerID_RegKeys_PathList.append( fullKeyPath )
			#
			# END if ContainerID_ValueData.lower() == currentSubkey_ContainerID.lower() : ...

		#
		# END for j in range(count_SerialNum_subKeys) : ...

	#
	# END for i in range(count_Enum_USB_subKeys) : ...

	if len(Matching_ContainerID_RegKeys_PathList) == 0:
		ContainerID_Not_Found_Msg_Format = "ERROR -- Unable to find ContainerID that matches [{0}]!"
		ContainerID_Not_Found_Msg_String = ContainerID_Not_Found_Msg_Format.format( ContainerID_ValueData )
		print(ContainerID_Not_Found_Msg_String)
		return None
	#
	# END if len(Matching_ContainerID_RegKeys_PathList) == 0: ...

	if len(Matching_ContainerID_RegKeys_PathList) == 1:
		ContainerID_Found_Msg_Format = "Found a ContainerID matching [{0}]!  Attempting to open its 'Device Parameters' subkey..."
		ContainerID_Found_Msg_String = ContainerID_Found_Msg_Format.format( ContainerID_ValueData )
		print(ContainerID_Found_Msg_String)

		Matching_ContainerID_DevParams_RegKey_Path = Matching_ContainerID_RegKeys_PathList[0] + "\\" + "Device Parameters"

		RegKey_Matching_SerialNum_DevParams_subkey = winreg.OpenKey( RegHive_HKLM,
															 Matching_ContainerID_DevParams_RegKey_Path )

		(symbolicName_ValueData, symbolicName_ValueType) = \
			winreg.QueryValueEx(RegKey_Matching_SerialNum_DevParams_subkey, "SymbolicName")

		SymbolicName_Found_Msg_Format = "Found SymbolicName [{0}] for device [{1}]!  Returning..."
		SymbolicName_Found_Msg_String = SymbolicName_Found_Msg_Format.format( symbolicName_ValueData, driveLetter )
		print(SymbolicName_Found_Msg_String)

		return symbolicName_ValueData
	#
	# END for i in range(count_Enum_USB_subKeys) : ...

	if len(Matching_ContainerID_RegKeys_PathList) > 1:
		Multiple_ContainerIDs_Found_Msg_Format = "ERROR -- Found more than one ContainerID that matches [{0}]!"
		Multiple_ContainerIDs_Found_Msg_String = Multiple_ContainerIDs_Found_Msg_Format.format( ContainerID_ValueData )
		print(Multiple_ContainerIDs_Found_Msg_String)
		return None
	#
	# END if len(Matching_ContainerID_RegKeys_PathList) == 0: ...

	print("ERROR -- Unexpected error!  [Reached the end of Convert_DriveLetter_To_USB_Info()?!]")
	return None
#
# END Convert_DriveLetter_To_USB_Info(driveLetter) :



if __name__ == "__main__":

	print("PyWin_DriveLetter_To_USB_Info:  Initializing...")


	parser = argparse.ArgumentParser(	prog="PyWin_DriveLetter_To_USB_Info",
										description="Converts a drive letter to its USB VID (Vendor ID), PID (Product ID), and Serial Number.")

	parser.add_argument( "USB_DriveLetter",
						 help="The drive letter (e.g. \"E:\") of the USB drive that you want to convert to its VID/PID/Serial." )

	args = parser.parse_args()

	if len(args.USB_DriveLetter) not in [1, 2] :
		lengthErrorMessageFormat = "Error:  USB_DriveLetter argument not the correct length!  (Argument was '{0}' -- expected a drive letter, e.g. \"E:\".)"
		lengthErrorMessageString = lengthErrorMessageFormat.format(args.USB_DriveLetter)
		print(lengthErrorMessageString)
		sys.exit(-10)
	#
	# END if len(args.USB_DriveLetter) not in [1, 2] : ...


	if (len(args.USB_DriveLetter) == 2) and (args.USB_DriveLetter[1] != ':') :
		formatErrorMessageFormat = "Error:  USB_DriveLetter argument not the correct format!  (Argument was '{0}' -- expected a drive letter, e.g. \"E:\".)"
		formatErrorMessageString = formatErrorMessageFormat.format(args.USB_DriveLetter)
		print(formatErrorMessageString)
		sys.exit(-20)
	#
	# END if (len(args.USB_DriveLetter) == 2) and (args.USB_DriveLetter[1] != ':') : ...


	if args.USB_DriveLetter[0] not in string.ascii_letters:
		letterErrorMessageFormat = "Error:  USB_DriveLetter argument not a valid letter!  (Argument was '{0}' -- expected a drive letter, e.g. \"E:\".)"
		letterErrorMessageString = letterErrorMessageFormat.format(args.USB_DriveLetter)
		print(letterErrorMessageString)
		sys.exit(-30)
	#
	# END if args.USB_DriveLetter[0] not in string.ascii_letters: ...


	if len(args.USB_DriveLetter) == 1:
		args.USB_DriveLetter += ":"
	#
	# END if len(args.USB_DriveLetter) == 1: ...

#	(Device_VID, Device_PID, Serial_Number, ObjectManager_Path) = \
#		Convert_DriveLetter_To_USB_Info(args.USB_DriveLetter)

	USB_dev_PNP_info = Convert_DriveLetter_To_USB_Info(args.USB_DriveLetter)

	C_drive_PNP_info = Convert_DriveLetter_To_USB_Info("C:")

	print("PyWin_DriveLetter_To_USB_Info:  All done!  :-)")
#
# END if __name__ == "__main__": ...






